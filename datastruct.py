"""
.. module:: datastruct
   :platform: OSX
   :synopsis: Data structures for the convex hull.

.. moduleauthor:: Jacques Dafflon <jacques.dafflon@usi.ch>

The datastruct module contains data structures the convex hull.
"""
__all__ = ["Point"]

class Point(object):
    """
    Representation of a 2D point.

    This class represents a point with two coordinates: x and y.

    Attributes:
        x (float): x coordinate of the point.
        y (float): y coordinate of the point.
    """
    def __init__(self, x, y):
        self._x = None
        self._y = None
        self.x = x
        self.y = y

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, val):
        self._x = val

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, val):
        self._y = val

    def dist_to(self, other):
        """
        Compute the square distance between this point and another point.

        Arguments:
            other (Point): Other point used to compute the distance.
        """
        return (other.x - self.x)*(other.x - self.x) + (other.y - self.y)*(other.y - self.y)

    def __repr__(self):
        # Integer representation for float without decimal
        x = int(self.x) if self.x.is_integer() else self.x
        y = int(self.y) if self.y.is_integer() else self.y
        return self.__class__.__name__ + "({}, {})".format(x, y)

    # Comparaison methods, precedence is given to the y coordinate.
    def __eq__(self, other):
        return (self.y, self.x).__eq__((other.y, other.x))

    def __ne__(self, other):
        return (self.y, self.x).__ne__((other.y, other.x))

    def __lt__(self, other):
        return (self.y, self.x).__lt__((other.y, other.x))

    def __gt__(self, other):
        return (self.y, self.x).__gt__((other.y, other.x))

    def __le__(self, other):
        return (self.y, self.x).__le__((other.y, other.x))

    def __ge__(self, other):
        return (self.y, self.x).__ge__((other.y, other.x))
