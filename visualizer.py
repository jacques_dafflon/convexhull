"""
.. module:: visualizer
   :platform: OSX
   :synopsis: Drawing functions for the convex hull.

.. moduleauthor:: Jacques Dafflon <jacques.dafflon@usi.ch>

The drawing module contains functions used to visualize the convex hull.
"""

__all__ = ["draw_hull"]

import time
import subprocess
import sys
import os

def draw_hull(hull_points, discarded, skipped, filename=None, show_labels=False,
    keep_dot=False, img=True):
    """
    Generate a dot file representing all the points and the convex hull.

    This function can either generate a dot file, a picture or both.

    Arguments:
        hull_points ([Point]): List of points on the convex hull, in counter
                               clockwise order.
        discarded ([Points]): List of points considered to be on the convex hull
                              but discarded because they were not.
        skipped ([Point]): Points known not to be on the convex hull and
                           therefore not considered.
        filename (str): Name of the file used for both the dot and png file.
                        If ``None``, the name will be
                        ``convex_hull-<num of points>-<time>``.
        show_labels (bool): Show the coordinates of the points.
                            Defaults to ``False``.
        keep_dot (bool): Wether to keep the dot file or not.
                         defaults to ``False``.
        img (bool): Wether to generate an image or not.
                    defaults to ``True``.
                    Note that this has precedence on ``keep_dot``. This means if
                    it is asked to keep neither the image nor the dot file, the
                    image will not be generated but the dot file will.
    """
    start_color = "color=darkgreen"
    start_opts = "{0},width=0.09,height=0.09".format(start_color)
    hull_color = "color=firebrick1"
    hull_opts = "{0},width=0.08,height=0.08".format(hull_color)
    discarded_color = "color=dodgerblue3"
    discarded_opts = "{0}".format(discarded_color)
    skipped_color = "color=goldenrod1"
    skipped_opts = "{0}".format(skipped_color)
    edge_opts = "arrowsize=0.4,arrowhead=vee"

    graph = [
        "digraph g {\n\tlayout=nop;\n\tnodesep=0;\n\tsplines=line;",
        "\n\tnode [shape=point,style=filled,fixedsize=true];\n"
    ]

    min_point = hull_points[0]

    # Points not considered by graham scan (skipped)
    for i in range(len(skipped)):
        graph.append("\t{0} [pos=\"{1},{2}!\",{3}];\n".format(
            len(discarded) + len(hull_points) + i + 1,
            skipped[i].x,
            skipped[i].y,
            skipped_opts)
        )

    # Points considered by graham scan but not part of the hull (skipped)
    for i in range(len(discarded)):
        graph.append("\t{0} [pos=\"{1},{2}!\",{3}];\n".format(
            len(hull_points)+i+1,
            discarded[i].x,
            discarded[i].y,
            discarded_opts)
        )

    # Points part of the hull
    for i in range(1, len(hull_points)):
        graph.append(("\t{0} [pos=\"{1},{2}!\""
            + (",xlabel=\"{1},{2}\"" if show_labels else "")
            + ",{3}];\n").format(
            i + 1,
            hull_points[i].x,
            hull_points[i].y,
            hull_opts)
        )

    # Initial point on the hull (leftmost point with smallest y value)
    graph.append(("\t1 [pos=\"{0},{1}!\""
        + (",xlabel=\"{0},{1}\"" if show_labels else "")
        + ",{2}];\n").format(
        hull_points[0].x,
        hull_points[0].y,
        start_opts)
    )

    # Edges
    for i in range(1, len(hull_points)):
        graph.append("\t{0}->{1} [{2}];\n".format(i, i+1, edge_opts))
    graph.append("\t{0}->1 [{1}];\n".format(len(hull_points), edge_opts))

    # Legend
    label_y1 = min_point.y - 40
    label_y2 = label_y1 - 10
    label_x = min_point.x
    label_opts = "shape=plaintext,style=solid,labelloc=\"c\",width=3.5"
    node_opts = "shape=point,style=filled,width=0.08,height=0.08"
    graph.extend([
        # Legend
        "\tsubgraph clusterlegend {\n\t\t",
        "labelloc=\"t\";\n\t\tlabel=\"Legend\";\n\t\t",
	    "label[shape=none,label=\"Legend:\",labelloc=\"c\",style=solid,",
        "pos=\"{0},{1}!\"];\n\t\t".format(label_x-60,label_y1-5),
        # Start point
	    "startp [{0},{1},pos=\"{2},{3}!\"];\n\t\t"
        .format(start_color, node_opts, label_x, label_y1),
        # Start point legend
	    "slabel [label=\"start point\",{0},pos=\"{1},{2}!\"];\n\t\t"
        .format(label_opts, label_x, label_y2),
        # Hull point
	    "c_hull [{0},{1},pos=\"{2},{3}!\"];\n\t\t"
        .format(hull_color, node_opts, label_x+70,label_y1),
	    # Hull point legend
        "hlabel [label=\"hull point\",{0},pos=\"{1},{2}!\"];\n\t\t"
        .format(label_opts, label_x+70, label_y2),
        # Discarded point
	    "discar [{0},{1},pos=\"{2},{3}!\"];\n\t\t"
        .format(discarded_color, node_opts, label_x+145, label_y1),
	    # Discarded point legend
        "dlabel [label=\"discarded point\",{0},pos=\"{1},{2}!\"];\n\t\t"
        .format(label_opts, label_x+145, label_y2),
        # Skipped point
	    "ignore [{0},{1},pos=\"{2},{3}!\"];\n\t\t"
        .format(skipped_color, node_opts, label_x+230, label_y1),
	    # Skipped point legend
        "ilabel [label=\"skipped point\",{0},pos=\"{1},{2}!\"];\n\t"
        .format(label_opts, label_x+230,label_y2),
        "}\n}"
    ])

    # Save into a dot file and generate an image.
    filename = "{0}-{1}-{2}" .format(
        filename if filename else "convex_hull",
        len(hull_points) + len(skipped) + len(discarded),
        time.strftime("%Y_%m_%d-%H_%M_%S")
    )

    with open("{0}.dot".format(filename), 'w') as dotfile:
        dotfile.write("".join(graph))

    if not img:
        return None

    try:  # Generate dot file
        subprocess.check_call(["dot", "-Tpng", "-o", "{0}.png".format(filename),
            "{0}.dot".format(filename)])
    except subprocess.CalledProcessError:
        sys.stderr.write("Failed to generate image from dot file!\n")
    else:
        try:  # Open image
            subprocess.check_call(["open", "{0}.png".format(filename)])
        except subprocess.CalledProcessError:
            sys.stderr.write("Failed to display image!\n")
            sys.stderr.write("The dot file will be kept instead.\n")
        else:
            if not keep_dot:
                os.remove("{0}.dot".format(filename)) # clean up the dot file
    return None
