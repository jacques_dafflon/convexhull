.. Graham Scan documentation master file, created by
   sphinx-quickstart on Sun Nov 24 15:00:29 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Graham Scan's documentation!
#######################################

This is a simple program written in python to compute the convex
hull from a set of point using `Graham Scan <https://en.wikipedia.org/wiki/Graham_scan>`_
and visualize it with `dot <http://graphviz.org/>`_.

Credits
*******
Jacques Dafflon <jacques.dafflon@usi.ch>


Contents
********

.. toctree::
   :maxdepth: 2

   license
   readme
   doc


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

