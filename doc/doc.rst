Documentation
-------------
   	Official Documentation:

        This page contains the documentation for the functions used to compute
        and represent the convex hull.

chg
===
.. automodule:: chg
	:members:
	:undoc-members:
	:show-inheritance:

datastruct
==========
.. automodule:: datastruct
    :members:
    :undoc-members:
    :show-inheritance:

visualizer
==========
.. automodule:: visualizer
    :members:
    :undoc-members:
    :show-inheritance:
