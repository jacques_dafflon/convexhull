#! /usr/bin/env python3
"""
.. module:: chg
   :platform: OSX
   :synopsis: Compute the convex hull.

.. moduleauthor:: Jacques Dafflon <jacques.dafflon@usi.ch>

This is a simple program written in python to compute the convex hull
from a set of point using Graham Scan and visualize it with dot.
"""
import sys
import os
import getopt

from datastruct import Point
from visualizer import draw_hull

def cmp_to_key(mycmp, min_point):
    """
    Convert a cmp= function into a key= function for Python 3.

    Based on this `recipe <http://code.activestate.com/recipes/576653-convert-a-cmp-function-to-a-key-function/>`_
    to convert a Python 2 style `cmp` into a `key` for the built-in `sorted`
    function.
    """
    class CmpToKey(object):
        def __init__(self, obj, *args):
            self.obj = obj
        def __lt__(self, other):
            return mycmp(self.obj, min_point, other.obj) < 0
        def __gt__(self, other):
            return mycmp(self.obj, min_point, other.obj) > 0
        def __eq__(self, other):
            return mycmp(self.obj, min_point, other.obj) == 0
        def __le__(self, other):
            return mycmp(self.obj, min_point, other.obj) <= 0
        def __ge__(self, other):
            return mycmp(self.obj, min_point, other.obj) >= 0
        def __ne__(self, other):
            return mycmp(self.obj, min_point, other.obj) != 0
    return CmpToKey

def optimization(points, p1, p2, p3, p4, verbose=False):
    """
    Optimize an input for convex hull by pruning points.

    This function removes points from the ``points`` list (in place)
    and returns a list with the deleted points.

    The points removed are the one in the rectangle contained in the
    quadrilateral expressed by p1, p2, p3, p4.

    The optimization is based on this `article <http://mindthenerd.blogspot.ch/2012/05/fastest-convex-hull-algorithm-ever.html>`_.

    Arguments:
        p1 (Point) : bottom right point of the quadrilateral.
        p2 (Point) : top right point of the quadrilateral.
        p3 (Point) : top left point of the quadrilateral.
        p4 (Point) : bottom left point of the quadrilateral.
        verbose (bool): Wether to be verbose or not. Defaults to ``False``.

    Returns:
        [Point] : list of points which have been removed.
    """
    x1 = max(p3.x, p4.x)
    x2 = min(p1.x, p2.x)
    y1 = min(p3.y, p4.y)
    y2 = max(p1.y, p2.y)
    deleted = []
    for i in range(len(points)-1, -1, -1):
        if x1 < points[i].x < x2 and y1 < points[i].y < y2:
            if verbose:
                sys.stderr.write("Skipping {0}\n".format(points[i]))
            deleted.append(points[i])
            del points[i]
    return deleted

def orientation(p1, p2, p3):
    """
    Orientation test between three points.

    Arguments:
        p1 (Point): First point used ofr the orientation test.
        p2 (Point): Second point used ofr the orientation test.
        p3 (Point): Third point used ofr the orientation test.

    Returns:
        int: A value matching one of the attribute and indicates which type of
        turn there is between the points.

    Attributes:
        LEFT (int): Value of a left turn between three points
        LINEAR (int): Value for aligned points (no turn).
        RIGHT (int): Value of a left turn between three points
    """
    orientation.LEFT  = 1
    orientation.LINEAR = 0
    orientation.RIGHT = -1
    cmp = (p2.x - p1.x)*(p3.y - p1.y) - (p3.x - p1.x)*(p2.y - p1.y)
    return (cmp > 0) - (cmp < 0)

def convex_hull(points, min_point, verbose=False):
    """
    Graham scan implementation to compute a convex hull.

    Arguments:
        points ([Point]): List of points to consider for the convex hull.
                          The list should not include the min point.
        min_point (Point): Point with the lowest y coordinate.
                           In case of a tie, it should be the leftmost one.
        verbose (bool): Wether to be verbose or not. Defaults to ``False``.

    Returns:
        Three lists of points: stack, discarded, ignored.

        #. ``stack``: List all the points on the convex hull in counter
                      clockwise order
        #. ``discarded``: List the points considered to be on the convex hull
                          and later discarded because they were not.
        #. ``skipped``: List of points not considered to be on the convex hull
                        because they have the same angle as other points with
                        ``min_point`` but are closer to it.
    """
    # Sort point using the orientation.
    points.sort(key=cmp_to_key(orientation, min_point))
    skipped = []
    discarded = []
    # Remove points with equal polar angle except the farthest one.
    for i in range(len(points)-2, -1, -1):
        if orientation(points[i], min_point, points[i+1]) == orientation.LINEAR:
            if min_point.dist_to(points[i]) < min_point.dist_to(points[i+1]):
                if verbose:
                    sys.stderr.write("Skipping {0}\n".format(points[i]))
                skipped.append(points[i])
                del points[i]
            else:
                if verbose:
                    sys.stderr.write("Skipping {0}\n".format(points[i+1]))
                skipped.append(points[i+1])
                del points[i+1]
    stack = [min_point]  # Create a stack and push the min_point.

    # Build the convex hull
    for point in points:
        while (len(stack) > 1
            and orientation(stack[-2], stack[-1], point) != orientation.LEFT):
            discarded.append(stack.pop())
        if not stack or stack[-1] != point: # avoid degenerate input
            stack.append(point)
    return stack, discarded, skipped

def usage():
    """
    Print the usage of the program for help.
    """
    msg = [
        "This is a simple program written in python to compute the convex hull",
        "from a set of point using Graham Scan and visualize it with dot.",
        "",
        "SYNOPSIS:",
        "",
        "\t./chg.py [-hvodif<file>]",
        "\t./chg.py [--help, --verbose, --optimize, --dot, --image, --file <file>]",
        "",
        "OPTIONS:",
        "\t-h, --help\t\tDisplay this message.",
        "\t-v, --verbose\t\tVerbose output to stderr, sdout only has the points on the hull.",
        "\t-o, --optimize\t\tOptimization by pruning some of the point before computeing the hull.",
        "\t-d, --dot\t\tGenerate a dot file of the convex hull.",
        "\t-i, --image\t\tGenerate a png image of the convex hull.",
        "\t-f, --file\t\tFile to read the input. If not specifed, read from stdin."
    ]
    print("\n".join(msg))

if __name__ == '__main__':
    try:
        options, leftover = getopt.gnu_getopt(sys.argv[1:], "hvodif:",
            ['help', 'verbose', 'optimize', 'dot', 'image', 'file='])
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit(2)

    OPTIMIZE = False
    VERBOSE = False
    DOT = False
    IMG = False
    FILENAME = None

    if leftover:
        print("{0} is not a valid option and will be ignored.".format(leftover))

    for opt, arg in options:
        if opt in ('-h', '--help'):
            usage()
            sys.exit(0)

        elif opt in ('-v', '--verbose'):
            VERBOSE = True

        elif opt in ('-o', '--optimize'):
            OPTIMIZE = True

        elif opt in ('-d', '--dot'):
            DOT = True

        elif opt in ('-i', '--image'):
            IMG = True

        elif opt in ('-f', '--file'):
            FILENAME = arg
        else:
            print("Unknown option:", opt, arg)
            usage()
            sys.exit(2)

    all_points = []
    ignored = []
    min_point = None
    A = None; B = None; C = None; D = None
    lineno = 1

    if FILENAME is not None:
        with open(FILENAME, 'r') as input_file:
            input_data = []
            for line in input_file:
                input_data.append(line.rstrip())
    else:
        input_data = sys.stdin
    # Read points from input
    for line in input_data:
        try:
            point = Point(*map(float, line.strip().split()))
        except (TypeError, ValueError) as err:
            sys.stderr.write("ERROR! Invalid input:\n")
            sys.stderr.write("Could not convert \"{}\" to x,y coordinates.\n"
                .format(line.strip()))
            sys.stderr.write("The point will be ignored (line {})\n"
                .format(lineno))
        else:
            if OPTIMIZE:
                if A is None or point.x-point.y > A.x-A.y:
                    A = point
                if B is None or point.x+point.y > B.x+B.y:
                    B = point
                if C is None or point.x-point.y < C.x-C.y:
                    C = point
                if D is None or point.x+point.y < D.x+D.y:
                    D = point

            # Updating the min point
            if min_point is None:
                min_point = point
            # First point or smaller point
            elif point < min_point:
                all_points.append(min_point)
                min_point = point
            # Filter some duplicates (all of them if the input is sorted)
            elif point == min_point:
                sys.stderr.write("Skipping {0}\n".format(point))
                ignored.append(point)
            else:
                all_points.append(point)
        finally:
            lineno += 1

    # Optimization to remove some points not part of the hull. (in linear time)
    if OPTIMIZE:
        ignored.extend(optimization(all_points, A, B, C, D, VERBOSE))
    if VERBOSE:
        sys.stderr.write("Considering {0} points\n".format(len(all_points)))

    # Computing the convex hull
    hull, popped, discarded = convex_hull(all_points, min_point, VERBOSE)

    # Printing the hull points.
    for p in hull:
        print("{} {}".format(p.x, p.y))

    # Drawing the hull, using dot.
    if DOT or IMG:
        ignored.extend(discarded)
        draw_hull(hull, popped, ignored, filename=os.path.splitext(FILENAME)[0],
            keep_dot=DOT, img=IMG)
